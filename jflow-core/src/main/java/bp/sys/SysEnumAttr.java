package bp.sys;
/**
 sss
*/
public class SysEnumAttr
{
	/** 
	 标题  
	*/
	public static final String Lab = "Lab";
	/** 
	 Int key
	*/
	public static final String IntKey = "IntKey";
	/** 
	 EnumKey
	*/
	public static final String EnumKey = "EnumKey";
	/** 
	 Language
	*/
	public static final String Lang = "Lang";
	/** 
	 OrgNo
	*/
	public static final String OrgNo = "OrgNo";
	/** 
	 关联主键
	*/
	public static final String RefPK = "RefPK";
}